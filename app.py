from importlib.resources import path
from pathlib import Path
from flask import Flask, request
from google_token import GoogleToken
from camera_controller import CameraController

app = Flask(__name__)

googletoken = GoogleToken(is_debug=True)
camera_controller = CameraController()


@app.route('/')
def hello_world():  # put application's code here
    return 'Hello World!'


@app.route('/startup')
def startup():
    return camera_controller.startup()


@app.route('/name_latest_image', methods=["POST"])
def name_latest_image_post():
    name = request.json['name']
    print('Set name post:' + name)
    camera_controller.name_latest_image(name)
    return 'UPDATE_NAME_COMPLETED'


@app.route('/get_token/<client_name>')
def get_token(client_name):
    path_str = 'client/' + client_name + '.json'
    file_path = Path(path_str)
    print('GET_TOKEN')
    print(path_str)
    if file_path.is_file():
        return googletoken.get_google_token(path_str)
    else:
        return 'NAME_INVALID'


@app.route('/get_base64_picture')
def get_base64_image():
    return camera_controller.take_base64_image()


@app.route('/get_base64_sample_picture')
def get_base64_sample_picture():
    return camera_controller.take_base64_sample()


if __name__ == '__main__':
    app.run()
