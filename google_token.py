from google.oauth2 import service_account
import google.auth.transport.requests
import logging


class GoogleToken:
    def __init__(self, is_debug):
        self.is_debug = is_debug
        self.user_file_path = ''

    def get_google_token(self, user_file_path):
        self.user_file_path = user_file_path
        scopes = ['https://www.googleapis.com/auth/sqlservice.admin', 'https://www.googleapis.com/auth/cloud-platform']
        service_account_file_path = self.user_file_path
        credentials = service_account.Credentials.from_service_account_file(
            service_account_file_path, scopes=scopes)
        request = google.auth.transport.requests.Request()
        credentials.refresh(request)
        result = ''
        if credentials.valid:
            result = credentials.token
        else:
            result = 'REQUEST_INVALID'
        return result


