import base64
import subprocess
import time
import os
import json


def image_to_base64(file_path):
    print('Start convert to base64')
    with open(file_path, "rb") as image_file:
        # Read as binary data
        image_binary = image_file.read()
        # Encode to base64
        base64_data = base64.b64encode(image_binary)
        # Convert bytes to UTF-8 string
        base64_string = base64_data.decode("utf-8")
        return base64_string


class CameraController:
    def __init__(self):
        self.device_name = ''
        self.is_debug = True
        self.resolution = []
        self.output_path = ''
        self.pixel_format = ''
        self.extension = ''
        self.file_name = 'Capture'

    def startup(self):
        # Open json file
        with open('config/camera_config.json', 'r') as file:
            data = json.loads(file.read())
        # Save config
        self.device_name = data['device_name']
        self.resolution = [data['resolution_x'], data['resolution_y']]
        self.output_path = data['output_path']
        self.is_debug = data['is_debug']
        self.pixel_format = data['pixel_format']
        self.extension = data['extension']
        print('Setup completed')
        print(self.device_name)
        print(self.resolution)
        print(self.output_path)
        print(self.is_debug)
        print(self.pixel_format)
        print(self.extension)
        return data

    def take_base64_image(self):
        print('Take image by device :' + self.device_name)
        print('Resolution :' + self.resolution[0] + ':' + self.resolution[1])
        # Take picture

        subprocess.run(['v4l2-ctl', '-d', self.device_name, '--set-fmt-video=width=' + self.resolution[0]
                        + ',height=' + self.resolution[1] + ',pixelformat='+self.pixel_format])
        subprocess.run(['v4l2-ctl', '-d', self.device_name, '--stream-mmap', '--stream-count=1', '--stream-to='
                        + self.output_path + self.file_name + self.extension])
        # Waiting for picture available
        while not os.path.exists(self.output_path + self.file_name + self.extension):
            time.sleep(1)
        # Convert to base64 str
        return image_to_base64(self.output_path + self.file_name + self.extension)

    def name_latest_image(self, name):
        # get current image path
        path = self.output_path + self.file_name + self.extension
        # Get the current epoch time
        current_epoch_time = int(time.time())
        # new path will be format to <id>_<time>.jpg
        new_path = self.output_path + name + '_' + str(current_epoch_time) + self.extension
        # set name
        os.rename(path, new_path)
        print(f"File renamed from '{path}' to '{new_path}'.")

    def take_base64_sample(self):
        print('Take sample image :' + self.device_name)
        print('Resolution :' + self.resolution[0] + ':' + self.resolution[1])
        return image_to_base64('test_image/seal2.jpg')
